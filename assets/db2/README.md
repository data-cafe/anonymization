<!--
 Copyright (c) 2022 Data Terrae
-->
# Testing the anonymisator with a DB2 database
- [Testing the anonymisator with a DB2 database](#testing-the-anonymisator-with-a-db2-database)
  - [Prerequisites :](#prerequisites-)
    - [Install Python dependencies](#install-python-dependencies)
  - [Local Deployment](#local-deployment)
  - [Cloud Deployment](#cloud-deployment)
    - [A - If you have a db2database already setup in a cluster](#a---if-you-have-a-db2database-already-setup-in-a-cluster)
    - [B - Deployment](#b---deployment)
    - [C - Connection](#c---connection)
    - [D - Populate db2 and pseudonymization](#d---populate-db2-and-pseudonymization)
  - [Check data at anytime of the process](#check-data-at-anytime-of-the-process)
      - [1. Connect to pods or local container](#1-connect-to-pods-or-local-container)
      - [2. Check in db2 shell:](#2-check-in-db2-shell)




[IBM DB2 LINUX INSTALL DOC](https://www.ibm.com/docs/en/db2/11.5?topic=system-linux)

## Prerequisites :
- python3
- Docker
- Kubernetes and tools ( for cloud deployment)



### Install Python dependencies

```
sudo pip install ibm_db
```

```
sudo pip install names
```
---
## Local Deployment

In ```anonymization/assets/db2``` Run : 
```
bash install-local-db2.sh 
```
**Don't panic db2 setup is taking a long time. (2-3 minutes)**

Cleanup Docker environment :
```
bash remove-docker.sh
```

---
## Cloud Deployment
### A - If you have a db2database already setup in a cluster
- Verify that your kubectl current-context matches the context where the db2 database is deployed
- Then jump to C and D.
### B - Deployment
In
```anonymization/assets/db2/deploy``` Run :
```
bash 0-install.sh
```
**It's take a lot of time to db2 to setup, so wait between 2 and 3 minutes before continuing.**
### C - Connection 
Here we go lets populate it:

First we need to access our service by forwarding pod's port:
```
sudo -E kubefwd svc -n db2-database
```
kubefwd should forward the service port to your 127.1.27.1

If it is another port, update the (```dsn_hn```) field in ```anonymization/assets/db2/generator.py``` and the (```host```) field in  ```anonymization/config.sample-db2.json ```
### D - Populate db2 and pseudonymization

In ``` anonymization/assets/db2``` Run :
```
python3 generator.py
```

Now let pseudonymize that :
In ```/anonymization``` Run :
```
./gradlew run --args="--config ../config.sample-db2.json"
```

Check again in db2 shell that all has been pseudonymized !

---
## Check data at anytime of the process

#### 1. Connect to pods or local container

- Local container :

```
docker exec -it db2server bash -c "su - db2inst1"
```
- Kubernetes pod :

```
kubectl exec -it <full-pod-name> -- /bin/bash
su - db2inst1
```
#### 2. Check in db2 shell:
```
db2 "connect to testdb
db2 "select * from person"
db2 "select * from details"
```