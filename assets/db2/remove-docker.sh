# Copyright (c) 2022 Data Terrae

# Delete db2server container
ID=$(docker ps -aqf "name=db2server")
docker stop $ID
docker rm $ID
docker system prune -f

# Delete Docker folder
rm -rf ./docker

printf "Docker environment successfully deleted \n"