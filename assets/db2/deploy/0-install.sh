# Copyright (c) 2022 Data Terrae

kubectl create ns db2-database
kubectl config set-context --current --namespace=db2-database
kubectl apply -f db2-deployment.yaml
