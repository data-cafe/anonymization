"""
 Copyright (c) 2022 Data Terrae
"""

import ibm_db
import time
from pickle import TRUE

import names 

records = 1000

dsn_dr='IBM DB2 ODBC DRIVER'
dsn_db='testdb'
dsn_hn='127.1.27.1'
dsn_pt='50000'
dsn_prt='TCPIP'
dsn_uid='db2inst1'
dsn_pw='password'

dsn = (
    "DRIVER={0};"
    "DATABASE={1};"
    "HOSTNAME={2};"
    "PORT={3};"
    "PROTOCOL={4};"
    "UID={5};"
    "PWD={6};").format(dsn_dr, dsn_db, dsn_hn, dsn_pt, dsn_prt, dsn_uid, dsn_pw)
try:
    conn = ibm_db.connect(dsn, "", "")

except Exception as e:
    print ("Unable to connect " )
    print (str(e))

#Drop table if exists
table_drop=ibm_db.exec_immediate(conn, "DROP TABLE Person IF EXISTS;")
table_drop=ibm_db.exec_immediate(conn, "DROP TABLE Details IF EXISTS;")
# Create table
table_creation=ibm_db.exec_immediate(conn, "CREATE TABLE Person ( id INT NOT NULL GENERATED ALWAYS AS IDENTITY, firstname VARCHAR(100) NOT NULL, lastname VARCHAR(100) NOT NULL, PRIMARY KEY(ID));" )
table_creation=ibm_db.exec_immediate(conn, "CREATE TABLE Details ( id INT NOT NULL GENERATED ALWAYS AS IDENTITY,  person_id INT NOT NULL,  email VARCHAR(100) NOT NULL,  FOREIGN KEY (person_id)   REFERENCES Person(id)    ON UPDATE RESTRICT    ON DELETE CASCADE,  PRIMARY KEY(ID)  );")

# Insert part
insert_start_time = time.time()
insert_page_size = 1000
insert_page_count = round(records / insert_page_size)
print(f"Insert {insert_page_count * insert_page_size} anonymised elements")

# Each page
for page in range(insert_page_count):
    table_insertion = ibm_db

    # Request
    request = "INSERT INTO Person (firstname, lastname) VALUES "

    # All values
    for item in range(insert_page_size):
        request += f" ('{names.get_first_name()}', '{names.get_last_name()}'),"

    # Finish
    request = request[:-1] + ";"

    # Insert a row of data
    table_insertion.exec_immediate(conn, request)



    print(f" {page + 1}/{insert_page_count}: {(page + 1) * insert_page_size} persons inserted in {round(time.time() - insert_start_time, 2)} sec", end="\r")

print("")

# Build email
if TRUE:
    table_insertion = ibm_db

    # Request
    request = """INSERT INTO Details (person_id, email)
SELECT id, firstname || '.' || lastname || '@fake.email' as email
FROM person;"""

    # Insert a row of data
    table_insertion.exec_immediate(conn, request)

    print(" details inserted from person")

print(
    f" {(page + 1) * insert_page_size} insertion finished in {round(time.time() - insert_start_time, 2)} sec")


ibm_db.close(conn)