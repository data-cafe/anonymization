#!/bin/bash

# Copyright (c) 2022 Data Terrae

# Save the actual path in order to run generator.py
workdir=$PWD

# Create docker deployment environment
mkdir docker && cp .env_list docker/
cd docker 
docker run -h db2server --name db2server --restart=always --detach --privileged=true  -p 50000:50000 --env-file .env_list -v ./docker ibmcom/db2
printf "waiting db2 setup...\n" 
sleep 150

# Populate database 
cd $workdir
python3 generator.py

# Pseudonymization 
cd ../..
./gradlew run --args="--config ../config.sample-db2.json"
cd $workdir/docker
