"""
 Copyright (c) 2022 Data Terrae
"""

import os
import sqlite3
import time
from pickle import TRUE

import names

file = 'demo.sqlite'
records = 1000

# Remove existing database
if os.path.exists(file):
    print("Delete existing database")
    os.remove(file)

# Create database
print("Create database")
con = sqlite3.connect(file)

# Create table
print("Create structure")

con.execute('''CREATE TABLE `Person` (
	`id`		INTEGER	NOT NULL UNIQUE,
	`firstname`	TEXT	DEFAULT NULL,
	`lastname`	TEXT	DEFAULT NULL,
	PRIMARY KEY(`id` AUTOINCREMENT)
);''')

con.execute('''CREATE TABLE `Details` (
	`id`		INTEGER	NOT NULL UNIQUE,
	`email`		TEXT,
	`person_id`	INTEGER	NOT NULL UNIQUE,
	FOREIGN KEY(`person_id`) REFERENCES `Person`(`id`),
	PRIMARY KEY(`id` AUTOINCREMENT)
);''')

# Save (commit) the changes
con.commit()


# Insert part
insert_start_time = time.time()
insert_page_size = 1000
insert_page_count = round(records / insert_page_size)
print(f"Insert {insert_page_count * insert_page_size} anonymised elements")

# Each page
for page in range(insert_page_count):
    cur = con.cursor()

    # Request
    request = "INSERT INTO Person (`firstname`, `lastname`) VALUES "

    # All values
    for item in range(insert_page_size):
        request += f" ('{names.get_first_name()}', '{names.get_last_name()}'),"

    # Finish
    request = request[:-1] + ";"

    # Insert a row of data
    cur.execute(request)

    # Save (commit) the changes
    con.commit()

    print(f" {page + 1}/{insert_page_count}: {(page + 1) * insert_page_size} persons inserted in {round(time.time() - insert_start_time, 2)} sec", end="\r")

print("")

# Build email
if TRUE:
    cur = con.cursor()

    # Request
    request = """INSERT INTO `Details` (`person_id`, `email`)
SELECT `id`, firstname || '.' || lastname || '@fake.email' as `email`
FROM `person`;"""

    # Insert a row of data
    cur.execute(request)

    # Save (commit) the changes
    con.commit()

    print(" details inserted from person")

print(
    f" {(page + 1) * insert_page_size} insertion finished in {round(time.time() - insert_start_time, 2)} sec")

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
con.close()
