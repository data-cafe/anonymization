/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.anonymizator;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cafe.data.anonymization.config.v1.Config;
import cafe.data.anonymization.flow.Field;
import cafe.data.anonymization.flow.FieldType;
import cafe.data.anonymization.flow.Item;
import cafe.data.anonymization.flow.Topology;

public class Anonymizator {
    private static final Logger LOGGER = LogManager.getLogger(Anonymizator.class);

    private Map<FieldType, Generator> ruleGenerators;

    // Lifecycle ---------------------------------------------------------------

    public Anonymizator() {
        LOGGER.debug("Creating...");
    }

    public void init(Config config, List<Topology> topologies) {
        LOGGER.debug("Initializing...");

        // List rules
        // Create rule generators
        ruleGenerators = topologies
                .parallelStream()
                .map(Topology::getAnonFields)
                .flatMap(List::stream)
                .map(Field::getType)
                .flatMap(GeneratorHelper::streamWithDependencies)
                .distinct()
                .collect(Collectors.toMap(
                        Function.identity(), // Key: rule
                        GeneratorHelper::fromRule)); // Value: generator

        // Init generators
        ruleGenerators.values()
                .parallelStream()
                .forEach(generator -> generator.init(config));
    }

    public void destroy() {
        LOGGER.debug("Destroying...");

        // Destroy rules
        ruleGenerators.values().forEach(Generator::destroy);
        ruleGenerators.clear();
    }

    // Public methods ----------------------------------------------------------

    /**
     * Anonymize the given items.
     * 
     * The field contains:
     * - the field name in DB (original value)
     * - the rule to apply
     * - the field name in ephimeral table (anonymized value)
     * 
     * The item contains:
     * - the original value from DB
     * - the anonymized value
     * 
     * Note: the anonymized value can be shared among original fields.
     * 
     * @param item
     * @param field
     */
    public void anonymize(Collection<Item> items) {
        items.forEach(item -> {
            item.getTopology().getAnonFields().forEach(field -> {
                this.anonymize(item, field);
            });
        });

    }

    /**
     * Anonymize the given item.
     * 
     * The field contains:
     * - the field name in DB (original value)
     * - the rule to apply
     * - the field name in ephimeral table (anonymized value)
     * 
     * The item contains:
     * - the original value from DB
     * - the anonymized value
     * 
     * Note: the anonymized value can be shared among original fields.
     * 
     * @param item
     * @param field
     */
    public void anonymize(Item item, Field field) {
        ruleGenerators
                .get(field.getType())
                .anonymize(item, field, this);

    }
}
