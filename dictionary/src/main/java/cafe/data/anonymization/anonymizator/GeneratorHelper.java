/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.anonymizator;

import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cafe.data.anonymization.anonymizator.generator.EmailGenerator;
import cafe.data.anonymization.anonymizator.generator.FirstNameGenerator;
import cafe.data.anonymization.anonymizator.generator.LastNameGenerator;
import cafe.data.anonymization.flow.FieldType;

public class GeneratorHelper {
    private static final Logger LOGGER = LogManager.getLogger(GeneratorHelper.class);

    static public Generator fromRule(FieldType type) {
        switch (type) {
            case FIRST_NAME:
                return new FirstNameGenerator();
            case LAST_NAME:
                return new LastNameGenerator();
            case EMAIL:
                return new EmailGenerator();
            case PHONE:
                throw new RuntimeException("PHONE rule not implemented");
            case IDENTIFY:
                throw new RuntimeException("IDENTIFY rule is not allowed here");
            default:
                throw new RuntimeException("Unknown rule: " + type);
        }
    }

    private GeneratorHelper() {
        throw new IllegalStateException("Utility class");
    }

    static public Stream<FieldType> streamWithDependencies(FieldType type) {
        switch (type) {
            case EMAIL:
                return Stream.of(type, FieldType.FIRST_NAME, FieldType.LAST_NAME);
            case IDENTIFY:
                LOGGER.fatal("IDENTIFY rule is not allowed here; but we're nice and removing it to avoid errors.");
                return Stream.empty();
            default:
                return Stream.of(type);
        }
    }
}
