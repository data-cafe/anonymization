/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.anonymizator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cafe.data.anonymization.config.v1.Config;
import cafe.data.anonymization.flow.Field;
import cafe.data.anonymization.flow.Item;

public abstract class Generator {

    // Properties --------------------------------------------------------------

    protected final Logger LOGGER;

    private Config config;

    // Lifecycle ---------------------------------------------------------------

    protected Generator(Class<? extends Generator> clazz) {
        this.LOGGER = LogManager.getLogger(clazz);
    }

    public void init(Config config) {
        LOGGER.debug("Initializing...");
        this.config = config;
    }

    public void destroy() {
        LOGGER.debug("Destroying...");

        // Unlink config
        this.config = null;
    }

    // Getter/Setter -----------------------------------------------------------

    public Config getConfig() {
        return config;
    }

    // Public API --------------------------------------------------------------

    public abstract void anonymize(Item item, Field field, Anonymizator anonymizator);

}
