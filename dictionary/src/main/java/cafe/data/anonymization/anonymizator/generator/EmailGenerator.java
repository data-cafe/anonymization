/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.anonymizator.generator;

import org.apache.commons.lang3.StringUtils;

import cafe.data.anonymization.anonymizator.Anonymizator;
import cafe.data.anonymization.anonymizator.Generator;
import cafe.data.anonymization.flow.Field;
import cafe.data.anonymization.flow.Item;

public class EmailGenerator extends Generator {

    /**
     * Fake domain used to build email address.
     * 
     * Sample of web service allowing to see spam emails:
     * - Yopmail (yopmail.com, yopmail.fr, ...)
     * - Fakemail (fakemail.net, fakemail.org, foreastate.com, ...)
     * - Tempr.email (tempr.email, tempr.com, discard.email, ...)
     */
    private static final String FAKE_DOMAIN = "discard.email";

    public EmailGenerator() {
        super(EmailGenerator.class);
    }

    @Override
    public void anonymize(Item item, Field field, Anonymizator anonymizator) {
        // Prerequisite: first and last names
        item.computeAnonymizedValuesIfAbsent(Field.FIRST_NAME, anonymizator::anonymize);
        item.computeAnonymizedValuesIfAbsent(Field.LAST_NAME, anonymizator::anonymize);

        // Anonymize email
        item.computeAnonymizedValuesIfAbsent(field, () -> StringUtils.toRootLowerCase(
                item.getAnonymizedValue(Field.FIRST_NAME) + "."
                        + item.getAnonymizedValue(Field.LAST_NAME) + "."
                        + item.getIdentifier()
                        + field.getSelect().map(s -> "+" + s).orElse("")
                        + "@" + FAKE_DOMAIN));

    }

}
