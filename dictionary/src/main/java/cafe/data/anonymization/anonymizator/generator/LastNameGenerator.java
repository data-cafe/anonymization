/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.anonymizator.generator;

import org.ajbrown.namemachine.NameGenerator;
import org.ajbrown.namemachine.NameGeneratorOptions;

import cafe.data.anonymization.anonymizator.Anonymizator;
import cafe.data.anonymization.anonymizator.Generator;
import cafe.data.anonymization.config.v1.Config;
import cafe.data.anonymization.flow.Field;
import cafe.data.anonymization.flow.Item;

public class LastNameGenerator extends Generator {

    private NameGenerator generator;
    private NameGeneratorOptions options;

    public LastNameGenerator() {
        super(LastNameGenerator.class);
    }

    @Override
    public void init(Config config) {
        super.init(config);

        // Generator
        this.generator = new NameGenerator();

        // Options
        this.options = new NameGeneratorOptions();
        this.options.setGenderWeight(50); // set female probability of 50%
        // TODO: set seed
    }

    @Override
    public void destroy() {
        // Unlink generator
        this.generator = null;
        this.options = null;

        // Super destroy
        super.destroy();
    }

    @Override
    public void anonymize(Item item, Field field, Anonymizator anonymizator) {
        item.computeAnonymizedValuesIfAbsent(field, () -> this.generator.generateName().getLastName());
    }

}
