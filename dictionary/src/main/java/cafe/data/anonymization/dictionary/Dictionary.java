/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.dictionary;

import java.util.List;

import com.google.common.collect.Lists;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cafe.data.anonymization.anonymizator.Anonymizator;
import cafe.data.anonymization.config.v1.Config;
import cafe.data.anonymization.config.v1.DictionaryConfig;
import cafe.data.anonymization.flow.Item;
import cafe.data.anonymization.flow.Topology;
import cafe.data.anonymization.persistor.Persistor;
import cafe.data.anonymization.persistor.SQLitePersistor;

public class Dictionary {
    private static final Logger LOGGER = LogManager.getLogger(Dictionary.class);

    // Internal properties -----------------------------------------------------

    private DictionaryConfig config;
    private Persistor persistor;
    private final Anonymizator anonymizator;

    // Lifecycle ---------------------------------------------------------------

    public Dictionary() {
        LOGGER.debug("Creating...");
        this.anonymizator = new Anonymizator();
    }

    public void init(Config config, List<Topology> topologies) {
        LOGGER.debug("Initializing...");

        // Config
        this.config = config.getDictionary();

        // Persistor
        this.persistor = new SQLitePersistor(config, topologies);

        // Anonymizator
        this.anonymizator.init(config, topologies);
    }

    public void destroy() {
        LOGGER.debug("Destroying...");

        // Persistor
        this.persistor.destroy();
        this.persistor = null;

        // Unlink config
        this.config = null;
    }

    // Public methods ----------------------------------------------------------

    public void anonymize(List<Item> items) {

        // Read (load) items from dictionary (if they exist)
        Lists.partition(items, config.getPageSizeRead()).forEach(persistor::load);

        // Anonymize items
        anonymizator.anonymize(items);

        // Persist changes
        Lists.partition(items, config.getPageSizeWrite()).forEach(persistor::save);
    }

    public int getQueuedActions() {
        return this.persistor.getQueuedActions();
    }
}