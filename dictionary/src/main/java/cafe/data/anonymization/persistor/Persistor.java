/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.persistor;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cafe.data.anonymization.config.v1.Config;
import cafe.data.anonymization.config.v1.DictionaryConfig;
import cafe.data.anonymization.flow.Field;
import cafe.data.anonymization.flow.Item;
import cafe.data.anonymization.flow.Topology;

public abstract class Persistor {

    // Properties --------------------------------------------------------------

    protected final Logger LOGGER;

    protected final Config globalConfig;
    protected final DictionaryConfig config;
    protected final List<Topology> topologies;

    /**
     * De-doubloned fields by table (from topologies).
     */
    protected final Map<String, Collection<Field>> fieldsByTable = new HashMap<>();

    /**
     * Thread execurtor to update items
     */
    private final ThreadPoolExecutor executor;

    // Lifecycle ---------------------------------------------------------------

    protected Persistor(Class<? extends Persistor> clazz, Config config, List<Topology> topologies, int updateThreads) {
        // Specific logger
        this.LOGGER = LogManager.getLogger(clazz);
        LOGGER.debug("Creating...");

        // Link config
        this.globalConfig = config;
        this.config = config.getDictionary();
        this.topologies = topologies;

        // Executor
        this.executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(updateThreads);

        // De-doublon tables
        // De-doublon needed fields by table
        topologies.forEach(topology -> {
            String table = topology.getDictTable();

            // Get field list by table
            // - retrieve it if multiple topologies using same name
            // - create it if first typology of this kind
            fieldsByTable.computeIfAbsent(table, t -> new LinkedList<>());
            Collection<Field> fieldsInTable = fieldsByTable.get(table);
            Collection<Field> fieldsInThisTopology = topology.getFields(false, true, true);

            // Append topology fields in table (only if not already present)
            for (Field newTopologyField : fieldsInThisTopology) {
                Optional<Field> matchingTableField = fieldsInTable.stream().filter(newTopologyField::equals)
                        .findFirst();
                if (matchingTableField.isPresent()) {
                    // Already in table ? Nothing to do
                    // We just complete the persistance field
                    matchingTableField.get().completeWith(newTopologyField);
                } else {
                    // Not in table ? Add it
                    fieldsInTable.add(newTopologyField.clone());
                }
            }
        });

    }

    public void destroy() {
        LOGGER.debug("Destroying...");

        // Shutdown executor
        executor.shutdown();

        // Clean tables
        fieldsByTable.clear();
    }

    // Public API --------------------------------------------------------------

    /**
     * Load anonymezed items from dictionary.
     */
    public abstract void load(List<Item> items);

    public abstract List<Runnable> createPersisTask(List<Item> items);

    /**
     * Persist anonymezed items to dictionary.
     */
    public void save(List<Item> items) {
        // Remove untouched items
        List<Item> filteredItems = items.stream()
                .filter(Objects::nonNull)
                .filter(Item::isAnonymizationTouched)
                .collect(Collectors.toList());

        // Add items to update
        createPersisTask(filteredItems).stream()
                .filter(Objects::nonNull)
                .forEach(executor::execute);
        ;
    }

    public int getQueuedActions() {
        return executor.getQueue().size();
    }

    // Helper ------------------------------------------------------------------

}
