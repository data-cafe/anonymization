/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.persistor.helper;

public final class SQLHelper {

    /**
     * Encapsulate a value with back-quotes.
     */
    static public final String BACKQUOTE_ENCAPSULATOR = "`%s`";

    /**
     * Encapsulate a value with double-quotes.
     */
    static public final String DOUBLEQUOTE_ENCAPSULATOR = "\"%s\"";

    // Singleton ---------------------------------------------------------------

    private SQLHelper() {
        throw new IllegalStateException("Utility class");
    }
}
