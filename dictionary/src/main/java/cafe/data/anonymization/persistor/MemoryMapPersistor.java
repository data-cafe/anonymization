/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.persistor;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cafe.data.anonymization.config.v1.Config;
import cafe.data.anonymization.flow.Item;
import cafe.data.anonymization.flow.Topology;

public class MemoryMapPersistor extends Persistor {

    /**
     * Multiple dictionary:
     * 1. key: table name
     * 2. key: identifier
     * 3. key: field name
     * 4. value: anonymized value
     */
    private final Map<String, Map<String, Map<String, Object>>> storage = new HashMap<>();

    // Lifecycle ---------------------------------------------------------------

    public MemoryMapPersistor(Config config, List<Topology> topologies) {
        super(MemoryMapPersistor.class, config, topologies, 1);

        // Create tables
        this.fieldsByTable.keySet().forEach(table -> this.storage.put(table, new HashMap<>()));
    }

    @Override
    public void destroy() {
        // Clear storage
        this.storage.values().forEach(identifers -> {
            identifers.values().forEach(Map::clear);
            identifers.clear();
        });
        this.storage.clear();

        // Super
        super.destroy();
    }

    // Public API --------------------------------------------------------------

    @Override
    public void load(List<Item> items) {
        // Get table in memory
        Map<String, Map<String, Object>> db = this.storage.get(items.get(0).getTopology().getDictTable());

        // Load all anonymized values (even non relevant)
        items.stream()
                .filter(item -> db.containsKey(item.getIdentifier()))
                .forEach(item -> item.restoreAnonymizedValues(db.get(item.getIdentifier())));

    }

    @Override
    public List<Runnable> createPersisTask(List<Item> items) {
        return Arrays.asList(new Runnable() {
            @Override
            public void run() {
                // Find topology
                Topology topology = items.get(0).getTopology();

                // Get table in memory
                Map<String, Map<String, Object>> db = storage.get(topology.getDictTable());

                // Save all anonymized values (erase previous values)
                items.forEach(item -> {
                    db.computeIfAbsent(item.getIdentifier(), i -> new HashMap<>());
                    db.get(item.getIdentifier()).putAll(item.exportAnonymizedValues());
                });
            }
        });
    }

}
