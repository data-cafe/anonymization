/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.persistor;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cafe.data.anonymization.config.v1.Config;
import cafe.data.anonymization.flow.Field;
import cafe.data.anonymization.flow.Item;
import cafe.data.anonymization.flow.Topology;
import cafe.data.anonymization.persistor.helper.SQLHelper;

public class SQLitePersistor extends Persistor {

    /**
     * SQLite key for using in memory database (instead of a file).
     */
    static protected final String SQLITE_MEMORY_KEY = ":memory:";
    static protected final String SQLITE_PATH_DEFAULT = ".";
    static protected final String SQLITE_FILE_NAME = "dictionary.sqlite";

    static protected final String FIELD_IDENTIFIER = "identifier";

    static protected final String SQL_LIST_QUERY = "SELECT `name` FROM `sqlite_master` WHERE `type`=\"table\" AND `name` NOT LIKE \"sqlite_\";";

    static protected final String SQL_SCHEMA_QUERY = "PRAGMA table_info(`%s`);";

    static protected final String SQL_LOAD_QUERY = "SELECT `" + FIELD_IDENTIFIER + "`, %s FROM `%s` WHERE `"
            + FIELD_IDENTIFIER + "` IN (%s);";

    static protected final String SQL_CREATE_COLUMN = "`%s` TEXT, ";
    static protected final String SQL_CREATE_QUERY = "CREATE TABLE `%s` (`" + FIELD_IDENTIFIER
            + "` TEXT NOT NULL UNIQUE, %sPRIMARY KEY(`" + FIELD_IDENTIFIER + "`));";

    static protected final String SQL_ALTER_QUERY = "ALTER TABLE `%s` ADD COLUMN `%s` TEXT;";

    static protected final String SQL_SAVE_EXCLUDED_SET = "`%s`=excluded.`%s`";
    static protected final String SQL_SAVE_QUERY = "INSERT INTO `%s` (`" + FIELD_IDENTIFIER + "`, %s) " +
            "VALUES (\"%s\", %s) " +
            "ON CONFLICT(`" + FIELD_IDENTIFIER + "`) " +
            "DO UPDATE SET %s;";

    /**
     * Limit the query request time.
     * 
     * The timeout is in seconds.
     */
    protected static final int QUERY_TIMEOUT = 30;

    protected static final AtomicBoolean THREAD_PRIORITY = new AtomicBoolean(false);

    /**
     * Reusable connection to SQLite for whole treatment.
     */
    private Connection connection = null;

    /**
     * This collection is the stack of item to persist.
     */
    private final Map<String, Item> itemToPersist = Collections.synchronizedMap(new HashMap<>());

    // Lifecycle ---------------------------------------------------------------

    public SQLitePersistor(Config config, List<Topology> topologies) {
        super(SQLitePersistor.class, config, topologies, 1);

        // Get DB path
        String path = this.config.isInMemory() ? SQLITE_MEMORY_KEY : getPath();

        // Load SQlite driver
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:" + path);
        } catch (SQLException e) {
            LOGGER.error("Unable to connect to SQLite database", e);
        }

        // Init Database
        try {
            // Build query
            Statement statement = this.connection.createStatement();
            statement.setQueryTimeout(QUERY_TIMEOUT);

            // Get existing tables
            Set<String> exisitingTables = findExistingTablesAndFields(statement);
            LOGGER.debug("Existing tables: {}", exisitingTables);

            // Case 1: Table not already exists
            List<String> tableToCreate = createTableCreationQueries(exisitingTables);
            for (String query : tableToCreate) {
                LOGGER.debug("Creating table: {}", query);
                statement.execute(query);
            }

            // Case 2: Table already exists
            Map<String, Set<String>> existingStructures = findExistingStructures(statement, exisitingTables);
            List<String> tableAlterations = createTableAlterationQueries(existingStructures);
            for (String query : tableAlterations) {
                LOGGER.debug("Alter table: {}", query);
                statement.execute(query);
            }

        } catch (SQLException e) {
            // if the error message is "out of memory",
            // it probably means no database file is found
            LOGGER.error("Error communicating with database", e);
        }
    }

    @Override
    public void destroy() {
        LOGGER.debug("Destroying...");

        // Close DB connection
        if (this.connection != null) {
            try {
                this.connection.close();
            } catch (SQLException e) {
                LOGGER.error("Error closing DB connection", e);
            }
        }

        // Unload driver
        this.connection = null;

        // Remove files
        if (!this.config.isInMemory() && !this.config.isPerpetual()) {
            LOGGER.debug("Removing dictionary: {}", getPath());
            new File(getPath()).delete();
        }

        // Super
        super.destroy();
    }

    // Public API --------------------------------------------------------------

    @Override
    public void load(List<Item> items) {
        THREAD_PRIORITY.set(true);

        // Intercept items not yet persisted
        // true: items are in queue to persist
        // false: no item found in queue
        Map<Boolean, List<Item>> splittedItems = items.stream().collect(Collectors
                .groupingBy(item -> itemToPersist.containsKey(item.getIdentifier())));

        List<Item> itemsToMerge = splittedItems.get(true);
        List<Item> itemsToLoad = splittedItems.get(false);

        // Restore from waiting zone
        if (itemsToMerge != null && !itemsToMerge.isEmpty()) {
            for (Item item : itemsToMerge) {
                String identifier = item.getIdentifier();

                // Restore values found in the matching item in queue
                // inside this one
                item.restoreAnonymizedValues(itemToPersist.get(identifier));

                // Replace the matching item to persist by this new one
                // (because this one contains merged values and previous values)
                itemToPersist.put(identifier, item);
            }
        }

        // Restore from DB
        if (itemsToLoad != null && !itemsToLoad.isEmpty()) {
            try {
                loadItems(itemsToLoad);
            } catch (SQLException e) {
                LOGGER.error("Error loading data", e);
            }
        }

        THREAD_PRIORITY.set(false);
    }

    // Helper ------------------------------------------------------------------

    private String getPath() {
        return config.getPath().orElse(SQLITE_PATH_DEFAULT) + "/" + SQLITE_FILE_NAME;
    }

    /**
     * Request dictionary to find existing tables.
     * 
     * @param statement
     * @return A map with existing tables.
     * @throws SQLException
     */
    private Set<String> findExistingTablesAndFields(Statement statement)
            throws SQLException {
        Set<String> foundTables = new HashSet<>();
        ResultSet rs = statement.executeQuery(SQL_LIST_QUERY);
        while (rs.next()) {
            Optional.ofNullable(rs.getString("name"))
                    .filter(fieldsByTable::containsKey)
                    .ifPresent(foundTables::add);
        }
        LOGGER.debug("Found tables: {}", foundTables);
        return foundTables;
    }

    /**
     * Create queries to create tables if they do not exist.
     * 
     * @param foundTables The map of existing tables and their fields.
     * @return The list of queries to create tables if they do not exist.
     */
    private List<String> createTableCreationQueries(Set<String> foundTables) {
        return fieldsByTable
                .entrySet().parallelStream()
                // Filter tables that already exists
                .filter(entry -> !foundTables.contains(entry.getKey()))
                // Create query for each table
                .map(entry -> SQL_CREATE_QUERY.formatted(entry.getKey(), toQueryColumns(entry)))
                .toList();
    }

    private Map<String, Set<String>> findExistingStructures(Statement statement, Set<String> exisitingTables)
            throws SQLException {
        Map<String, Set<String>> foundStructures = new HashMap<>();
        for (String table : exisitingTables) {
            // Structure
            foundStructures.put(table, new HashSet<>());

            // Query
            String query = SQL_SCHEMA_QUERY.formatted(table);
            LOGGER.debug("Getting structure: {}", query);
            ResultSet rs = statement.executeQuery(query);

            // Fill structure
            while (rs.next()) {
                foundStructures.get(table).add(rs.getString("name"));
            }

            rs.close();
        }

        LOGGER.debug("Found structures: {}", foundStructures);
        return foundStructures;
    }

    /**
     * Create queries to alter tables to add new fields.
     * 
     * If all tables exists and all fields are already present, the returned list is
     * empty.
     * 
     * @param existingStructures The map of existing tables and their fields.
     * @return The list of queries to alter tables to add new fields.
     */
    private List<String> createTableAlterationQueries(Map<String, Set<String>> existingStructures) {

        return fieldsByTable
                .entrySet().parallelStream()
                .filter(entry -> existingStructures.containsKey(entry.getKey()))
                .map(entry -> {
                    String table = entry.getKey();
                    Set<String> wantedFields = fieldsToColumns(entry.getValue());
                    Set<String> existingFields = existingStructures.get(table);
                    wantedFields.removeIf(existingFields::contains);
                    return Map.entry(table, wantedFields);
                })
                .peek(entry -> LOGGER.debug("Table {} needs fields: {}", entry.getKey(), entry.getValue()))
                .filter(entry -> entry.getValue().size() > 0)
                .flatMap(entry -> entry.getValue().stream().map(f -> SQL_ALTER_QUERY.formatted(entry.getKey(), f)))
                .collect(Collectors.toList());
    }

    /**
     * Load anonymized data (for a subset of items).
     * 
     * @param topology
     * @param items
     * @param table
     * @param fields
     * @param statement
     * @throws SQLException
     */
    public void loadItems(List<Item> items) throws SQLException {

        Statement statement = this.connection.createStatement();
        statement.setQueryTimeout(QUERY_TIMEOUT);

        // Get metadata to request
        String table = items.get(0).getTopology().getDictTable();
        Collection<Field> fields = fieldsByTable.get(table);

        String flatFields = fields.stream()
                .map(Field::getAnonField)
                .map(SQLHelper.BACKQUOTE_ENCAPSULATOR::formatted)
                .collect(Collectors.joining(", "));

        String flatIdentifiers = items.stream()
                .map(Item::getIdentifier)
                .map(SQLHelper.DOUBLEQUOTE_ENCAPSULATOR::formatted)
                .collect(Collectors.joining(", "));

        String query = SQL_LOAD_QUERY.formatted(
                /* 1: fields */ flatFields,
                /* 2: table */ table,
                /* 3: identifiers */ flatIdentifiers);
        LOGGER.debug("Load: " + query);

        ResultSet rs = statement.executeQuery(query);
        while (rs.next()) {
            String identifier = rs.getString(FIELD_IDENTIFIER);
            Optional<Item> optItem = items.parallelStream()
                    .filter(item -> item.getIdentifier().equals(identifier))
                    .findFirst();
            if (optItem.isPresent()) {
                LOGGER.debug("Restoring {} for {}", identifier, flatFields);
                final Item item = optItem.get();
                for (Field field : fields) {
                    String value = rs.getString(field.getAnonField());
                    LOGGER.debug("Loading {}={} for item {}", field, value, identifier);
                    item.restoreAnonymizedValue(field, value);
                }
            }
        }

        statement.close();
    }

    @Override
    public List<Runnable> createPersisTask(List<Item> items) {
        // Link them in waiting list
        items.forEach(item -> itemToPersist.put(item.getIdentifier(), item));

        // Create tasks
        return items.parallelStream()
                .map(item -> new PersistTask(
                        connection,
                        itemToPersist,
                        item.getIdentifier(),
                        fieldsByTable.get(item.getTopology().getDictTable())))
                .collect(Collectors.toList());
    }

    // Helper ------------------------------------------------------------------

    private String toQueryColumns(Map.Entry<String, Collection<Field>> entry) {
        return fieldsToColumns(entry.getValue()).stream()
                .map(SQL_CREATE_COLUMN::formatted)
                .collect(Collectors.joining());
    }

    private Set<String> fieldsToColumns(Collection<Field> fields) {
        return fields.stream()
                .map(field -> field.isPersistable()
                        ? List.of(field.getAnonField(), field.getOrigField())
                        : List.of(field.getAnonField()))
                .flatMap(Collection::stream)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
    }
}

class PersistTask implements Runnable {
    private static final Logger LOGGER = LogManager.getLogger(PersistTask.class);

    private final Connection connection;
    private final Map<String, Item> itemToPersist;
    private final String identifier;
    private final Collection<Field> fields;

    public PersistTask(Connection connection, Map<String, Item> itemToPersist, String identifier,
            Collection<Field> fields) {
        this.connection = connection;
        this.itemToPersist = itemToPersist;
        this.identifier = identifier;
        this.fields = fields;
    }

    @Override
    public void run() {
        // Let priority to other tasks (read for instance)
        try {
            while (SQLitePersistor.THREAD_PRIORITY.get()) {
                // Sleep 50 ms
                Thread.sleep(50);
            }
        } catch (InterruptedException e) {
            LOGGER.info("Failed to interrupt this thread for priority task.");
            LOGGER.debug(e);
        }

        // Precondition: the item is still to persist
        if (!itemToPersist.containsKey(identifier)) {
            return;
        }

        // Pop item
        final Item item = itemToPersist.remove(identifier);

        // Get metadata to request
        String table = item.getTopology().getDictTable();
        Set<String> touchedKeys = item.getAnonymizationTouchedKeys();

        // Build SET part
        List<String> columns = new ArrayList<>();
        List<String> values = new ArrayList<>();
        List<String> assignations = new ArrayList<>(); // column = excluded.column

        // Note about upsert:
        // It would be like :
        // >> INSERT INTO table (columns)
        // >> VALUES (values)
        // >> ON CONFLICT (identifier)
        // >> DO UPDATE SET (assignations)
        // Note that the update part could either be:
        // - column = excluded.column (refers to the value in the insert part)
        // - column = value

        // Build SET part with anonymized values
        fields.stream()
                .filter(field -> touchedKeys.contains(field.getRuleField()))
                .filter(field -> item.getAnonymizedValue(field) != null)
                .forEach(field -> {
                    String column = field.getAnonField();
                    Object value = item.getAnonymizedValue(field);

                    // Insert: column and value (keep same order)
                    columns.add(SQLHelper.BACKQUOTE_ENCAPSULATOR.formatted(column));
                    values.add(SQLHelper.DOUBLEQUOTE_ENCAPSULATOR.formatted(value));

                    // Upsert: assignation
                    assignations.add(SQLitePersistor.SQL_SAVE_EXCLUDED_SET.formatted(column, column));
                });

        // Build SET part with anonymized values
        fields.stream()
                .filter(Field::isPersistable)
                .filter(field -> touchedKeys.contains(field.getRuleField()))
                .filter(field -> item.getOriginalValue(field) != null)
                .forEach(field -> {
                    String column = field.getOrigField();
                    Object value = item.getOriginalValue(field);

                    // Insert: column and value (keep same order)
                    columns.add(SQLHelper.BACKQUOTE_ENCAPSULATOR.formatted(column));
                    values.add(SQLHelper.DOUBLEQUOTE_ENCAPSULATOR.formatted(value));

                    // Upsert: assignation
                    assignations.add(SQLitePersistor.SQL_SAVE_EXCLUDED_SET.formatted(column, column));
                });

        // Build query
        if (assignations.size() > 0) {
            try {
                Statement statement = this.connection.createStatement();
                statement.setQueryTimeout(SQLitePersistor.QUERY_TIMEOUT);

                String query = SQLitePersistor.SQL_SAVE_QUERY.formatted(
                        /* 1: table */ table,
                        /* 2: columns */ String.join(", ", columns),
                        /* 3: identifier */ item.getIdentifier(),
                        /* 4: values */ String.join(", ", values),
                        /* 5: sets */ String.join(", ", assignations));

                LOGGER.debug("Save (upsert): " + query);
                statement.execute(query);
                statement.close();

            } catch (SQLException e) {
                LOGGER.error("Error saving data", e);
            }
        }
    }

}
