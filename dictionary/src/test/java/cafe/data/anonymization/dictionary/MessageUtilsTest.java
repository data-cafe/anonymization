/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.dictionary;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MessageUtilsTest {
    @Test
    void testGetMessage() {
        assertEquals("Hello World!", MessageUtils.getMessage());
    }
}
