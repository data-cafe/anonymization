/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.flow;

import java.util.List;

/**
 * Page information for pagination.
 */
public class Page {

    // Properties --------------------------------------------------------------

    /**
     * Page index where to start (included).
     */
    private int start;

    /**
     * Page index where to end (excluded).
     */
    private int end;

    // Constructors ------------------------------------------------------------

    public Page(int start, int end) {
        this.start = start;
        this.end = end;
    }

    // Getters -----------------------------------------------------------------

    /**
     * Get the first index (included).
     * 
     * @return the first index (included).
     */
    public int getStart() {
        return start;
    }

    /**
     * Get the last index (excluded).
     * 
     * @return the last index (excluded).
     */
    public int getEnd() {
        return end;
    }

    /**
     * Get the number of items in the page.
     * 
     * @return end - start
     */
    public int getSize() {
        return end - start;
    }

    // Items -------------------------------------------------------------------

    private List<Item> items;

    /**
     * Get the items.
     * 
     * @return the items.
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     * Set the items.
     * 
     * @param items the items.
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }

    // Public ------------------------------------------------------------------

    /**
     * Get the next page (for pagination) with the same page size but the next
     * interval.
     * 
     * @return The next interval.
     */
    public Page nextPage() {
        return new Page(end, end + end - start);
    }

}
