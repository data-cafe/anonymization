/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.flow;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.text.StringSubstitutor;
import org.apache.commons.text.lookup.StringLookup;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cafe.data.anonymization.config.v1.TopologyConfig;

public class Topology {
    private static final Logger LOGGER = LogManager.getLogger(Topology.class);

    static private StringSubstitutor ZEROIZER = new StringSubstitutor(new StringLookup() {
        public String lookup(String key) {
            return "0";
        }
    });

    static private Pattern FIELD_EXTRACTOR = Pattern.compile("\\$\\{(.+?)\\}");

    static public Topology fromConfig(TopologyConfig config) {
        // Fields to anonymize: from topology config
        List<Field> anonFields = config.getFields().parallelStream()
                .map(Field::fromConfig)
                .collect(Collectors.toList());

        // Fields to load from DB and persist to Ephemeral Dictionary
        List<Field> persistFields = anonFields.stream()
                .filter(Field::isPersistable)
                .collect(Collectors.toList());

        // Fields to identify: from template identifier
        List<Field> idFields = new LinkedList<>();
        Matcher matcher = FIELD_EXTRACTOR.matcher(config.getIdentifier());
        while (matcher.find()) {
            for (int i = 1; i <= matcher.groupCount(); i++) {
                String field = matcher.group(i);
                idFields.add(Field.fromPrimaryKey(field));
            }
        }
        if (idFields.size() == 0) {
            LOGGER.fatal("The list db field used to identify a row is empty; It will fail soon!");
        }

        // Create Topology
        return new Topology(
                config.getTable(),
                config.getIdentifier(), // like "id-{col_first}-{col_second}""
                ZEROIZER.replace(config.getIdentifier()), // like "id-0-0"
                idFields,
                anonFields,
                persistFields);
    }

    // Constructors ------------------------------------------------------------

    private Topology(String dbTable, String identifierTemplate, String dictTable, List<Field> idFields,
            List<Field> anonFields, List<Field> persistFields) {
        this.dbTable = dbTable;
        this.identifierTemplate = identifierTemplate;
        this.dictTable = dictTable;
        this.idFields = idFields;
        this.anonFields = anonFields;
        this.persistFields = persistFields;
    }

    // For Data Layer ----------------------------------------------------------

    private final String dbTable;

    /**
     * Get the database table.
     * 
     * @return the database table.
     */
    public String getDbTable() {
        return dbTable;
    }

    // For Ochestrator ---------------------------------------------------------

    private final String identifierTemplate;

    /**
     * Get the template used to compute the item identifier.
     * 
     * @return the template used to compute the item identifier.
     */
    public String getIdentifierTemplate() {
        return identifierTemplate;
    }

    // For Ephemeral Dictionary ------------------------------------------------

    private final String dictTable;

    public String getDictTable() {
        return dictTable;
    }

    // Fields ------------------------------------------------------------------

    private final List<Field> idFields;
    private final List<Field> anonFields;
    private final List<Field> persistFields;

    /**
     * Get the fields to identify each row in DB.
     * 
     * @return list of fields to identify item.
     */
    public List<Field> getIdFields() {
        return idFields;
    }

    /**
     * Get the fields to anonymize.
     * 
     * @return list of fields to anonymize.
     */
    public List<Field> getAnonFields() {
        return anonFields;
    }

    /**
     * Get the fields to persist in ephemeral dictionary.
     * 
     * @return list of fields to persist.
     */
    public List<Field> getPersistFields() {
        return persistFields;
    }

    /**
     * Get ALL fields to both identify each row and anonymize each values.
     * 
     * @return all fields needed to identify and anonymize.
     */
    public List<Field> getFields(boolean ids, boolean anons, boolean persists) {
        return Stream.of(
                ids ? idFields.stream() : Stream.<Field>empty(),
                anons ? anonFields.stream() : Stream.<Field>empty(),
                persists ? persistFields.stream() : Stream.<Field>empty())
                .flatMap(Function.identity())
                .distinct()
                .collect(Collectors.toList());
    }

}
