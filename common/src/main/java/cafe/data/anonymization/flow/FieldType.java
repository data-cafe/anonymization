/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.flow;

import java.util.Arrays;
import java.util.Optional;

import cafe.data.anonymization.config.v1.RuleConfig;
import cafe.data.anonymization.rules.DataType;

public enum FieldType {
    // Used to identify
    IDENTIFY(DataType.STRING),

    // Used to anonymize
    FIRST_NAME("first-name", DataType.STRING),
    LAST_NAME("last-name", DataType.STRING),
    EMAIL("email", DataType.STRING),
    PHONE("phone", DataType.STRING);

    // Properties --------------------------------------------------------------

    private Optional<String> dictionaryName;

    private DataType dataType;

    // Constructors ------------------------------------------------------------

    private FieldType(DataType type) {
        this.dictionaryName = Optional.empty();
        this.dataType = type;
    }

    private FieldType(String dictionaryName, DataType type) {
        this.dictionaryName = Optional.of(dictionaryName);
        this.dataType = type;
    }

    // Getters -----------------------------------------------------------------

    public Optional<String> getDictionaryName() {
        return dictionaryName;
    }

    public DataType getDataType() {
        return dataType;
    }

    // Reverse Lookup ----------------------------------------------------------

    public static Optional<FieldType> get(String rule) {
        return Arrays.stream(FieldType.values())
                .filter(env -> env.dictionaryName.filter(rule::equals).isPresent())
                .findFirst();
    }

    public static Optional<FieldType> fromConfig(RuleConfig rule) {
        switch (rule) {
            case EMAIL:
                return Optional.of(EMAIL);
            case FIRST_NAME:
                return Optional.of(FIRST_NAME);
            case LAST_NAME:
                return Optional.of(LAST_NAME);
            case PHONE:
                return Optional.of(PHONE);
            default:
                return Optional.empty();
        }
    }
}
