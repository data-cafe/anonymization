/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.flow;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

import com.google.common.collect.ImmutableMap;

import org.apache.commons.text.StringSubstitutor;
import org.apache.commons.text.lookup.StringLookup;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Item implements StringLookup {
    private static final Logger LOGGER = LogManager.getLogger(Item.class);

    /**
     * Create an item that encapsulates a row of data.
     * 
     * @param originalItem
     */
    public Item(Topology topology, Map<String, Object> originalItem) {
        this.topology = topology;
        this.originalValues = originalItem;
    }

    // Context: topology -------------------------------------------------------

    private final Topology topology;

    /**
     * Get the topology used to get this item.
     * 
     * @return the topology.
     */
    public Topology getTopology() {
        return topology;
    }

    // Item: our itentifier ----------------------------------------------------

    private String identifier;

    /**
     * Get the item identifier.
     * 
     * @return the item identifier.
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Compute identifier
     * 
     * @param identifier the item identifier.
     */
    public void computeIdentifier() {
        try {
            this.identifier = new StringSubstitutor(this).replace(topology.getIdentifierTemplate());
        } catch (IllegalArgumentException e) {
            LOGGER.error("Error computing identifier", e);
        }
    }

    // Item: original values in DB ---------------------------------------------

    private final Map<String, Object> originalValues;

    /**
     * Get the item original value for a specific field. This value is the one
     * stored originally in DB before anonymization.
     * 
     * Note: the expected field name is the one used as orgin DB column name.
     * 
     * @return the item value for field.
     */
    public Object getOriginalValue(String field) {
        return originalValues.get(field);
    }

    /**
     * Get the item original value for a specific field. This value is the one
     * stored originally in DB before anonymization.
     * 
     * @return the item value for field.
     */
    public Object getOriginalValue(Field field) {
        return originalValues.get(field.getDbField());
    }

    // Item: anonymized values -------------------------------------------------

    private final ConcurrentHashMap<String, Object> anonymizedValues = new ConcurrentHashMap<>();
    private final Set<String> anonymizedKeyTouched = new HashSet<>();

    public boolean hasAnonymizedValue(Field field) {
        return anonymizedValues.containsKey(field.getRuleField());
    }

    public Object getAnonymizedValue(Field field) {
        return anonymizedValues.get(field.getRuleField());
    }

    public void computeAnonymizedValuesIfAbsent(Field field, BiConsumer<Item, Field> anonymizer) {
        if (!anonymizedValues.containsKey(field.getRuleField())) {
            anonymizer.accept(this, field);
        }
    }

    public void computeAnonymizedValuesIfAbsent(Field field, Supplier<Object> anonymizer) {
        if (!anonymizedValues.containsKey(field.getRuleField())) {
            Object value = anonymizer.get();
            if (value != null && value != anonymizedValues.get(field.getRuleField())) {
                LOGGER.debug("PUT {}: {}={}", identifier, field.getRuleField(), value);
                anonymizedValues.put(field.getRuleField(), value);
                anonymizedKeyTouched.add(field.getRuleField());
            }
        }
    }

    public void restoreAnonymizedValues(Item item) {
        // Restore anonymized values
        anonymizedValues.putAll(item.anonymizedValues);
        anonymizedKeyTouched.addAll(item.anonymizedKeyTouched);

        // Also restore original values for persistance
        item.originalValues.entrySet().stream()
                .filter(entry -> entry.getValue() != null)
                .filter(entry -> this.originalValues.get(entry.getKey()) == null)
                .forEach(entry -> this.originalValues.put(entry.getKey(), entry.getValue()));
    }

    public void restoreAnonymizedValues(Map<String, Object> values) {
        if (values != null) {
            anonymizedValues.putAll(values);
        }
    }

    public void restoreAnonymizedValue(Field field, Object value) {
        if (value != null) {
            anonymizedValues.put(field.getRuleField(), value);
        }
    }

    public Map<String, Object> exportAnonymizedValues() {
        return ImmutableMap.copyOf(anonymizedValues);
    }

    public boolean isAnonymizationTouched() {
        return !anonymizedKeyTouched.isEmpty();
    }

    public Set<String> getAnonymizationTouchedKeys() {
        return anonymizedKeyTouched;
    }

    // Compatibility -----------------------------------------------------------

    @Override
    public String lookup(String key) {
        return getOriginalValue(key).toString();
    }

    @Override
    public String toString() {
        return "Item [" +
                "identifier=" + identifier + ", " +
                "originalValues=" + originalValues + ", " +
                "anonymizedValues=" + anonymizedValues +
                "]";
    }

}
