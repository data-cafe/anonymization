/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.flow;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import cafe.data.anonymization.config.v1.TypologyFieldConfig;

public class Field {

    static final public Field FIRST_NAME = new Field(FieldType.FIRST_NAME);
    static final public Field LAST_NAME = new Field(FieldType.LAST_NAME);

    static public Field fromConfig(TypologyFieldConfig config) {
        return new Field(config);
    }

    static public Field fromPrimaryKey(String primaryKeyField) {
        return new Field(primaryKeyField);
    }

    // Helper values -----------------------------------------------------------

    /**
     * Prefix for anonymized fields
     */
    private static final String PREFIX_ANON = "anon¤";

    /**
     * Prefix for values from origin
     */
    private static final String PREFIX_ORIG = "orig¤";

    // Constructors ------------------------------------------------------------

    /**
     * Create a new field from typology configuration.
     * 
     * @param config
     */
    private Field(TypologyFieldConfig config) {
        Optional<String> select = Optional
                .ofNullable(config.getSelect())
                .map(StringUtils::trimToNull);

        this.dbField = StringUtils.trimToEmpty(config.getField());
        this.persistable = config.isPersist();
        this.ruleField = select
                .map(s -> config.getRule().getName() + "¤" + config.getSelect())
                .orElseGet(() -> config.getRule().getName());
        this.anonField = PREFIX_ANON + this.ruleField;
        this.origField = config.isPersist() ? PREFIX_ORIG + this.ruleField : null;
        this.type = FieldType.fromConfig(config.getRule())
                .orElseThrow(() -> new IllegalArgumentException("Unknown rule: " + config.getRule()));
        this.select = select;
    }

    /**
     * Programmatic constructor for simple Rule.
     * 
     * Restrictions:
     * - cannot persist into ephemeral dictionary
     * 
     * @param type
     */
    private Field(FieldType type) {
        this.dbField = null;
        this.persistable = false;
        this.ruleField = type.getDictionaryName()
                .orElseThrow(() -> new IllegalArgumentException("Unallowed field type with this constructor: " + type));
        this.anonField = PREFIX_ANON + this.ruleField;
        this.origField = null; // not allowed to persist in dictionary
        this.type = type;
        this.select = Optional.empty();
    }

    /**
     * Programmatic constructor for primary key.
     * 
     * Restrictions:
     * - cannot anonymize
     * - cannot load anonymised value from ephemeral dictionary
     * - cannot persist into ephemeral dictionary
     * 
     * @param primaryKeyField
     */
    private Field(String primaryKeyField) {
        this.dbField = primaryKeyField;
        this.persistable = false;
        this.ruleField = null; // not allowed to anonymize
        this.anonField = null; // not allowed to restore from dictionary
        this.origField = null; // not allowed to persist in dictionary
        this.type = FieldType.IDENTIFY;
        this.select = Optional.empty();
    }

    /**
     * Programmatic constructor for clone
     * 
     * @param primaryKeyField
     */
    private Field(Field originalField) {
        this.dbField = originalField.dbField;
        this.persistable = originalField.persistable;
        this.ruleField = originalField.ruleField;
        this.anonField = originalField.anonField;
        this.origField = originalField.origField;
        this.type = originalField.type;
        this.select = originalField.select;
    }

    // Field (for DB origin) ---------------------------------------------------

    final private String dbField;

    public String getDbField() {
        return dbField;
    }

    // Field (for Flow process) ------------------------------------------------

    final private String ruleField;

    public String getRuleField() {
        return ruleField;
    }

    private boolean persistable;

    public boolean isPersistable() {
        return persistable;
    }

    // Field (for Ephemeral Dictionary) ----------------------------------------

    final private String anonField;
    final private String origField;

    public String getAnonField() {
        return anonField;
    }

    public String getOrigField() {
        return origField;
    }

    // Rule --------------------------------------------------------------------

    final private FieldType type;

    public FieldType getType() {
        return type;
    }

    // Select ------------------------------------------------------------------

    final private Optional<String> select;

    public Optional<String> getSelect() {
        return select;
    }

    // Helper methods ----------------------------------------------------------

    public Field clone() {
        return new Field(this);
    }

    public boolean equals(Field field) {
        // case 1: is identity
        return (this.type == FieldType.IDENTIFY && field.type == FieldType.IDENTIFY && this.dbField == field.dbField)
                // Case 2: is normal field
                || (this.type != FieldType.IDENTIFY && field.type != FieldType.IDENTIFY
                        && this.ruleField == field.ruleField && this.select == field.select);
    }

    public void completeWith(Field field) {
        if (field.persistable) {
            this.persistable = field.persistable;
        }
    }

}
