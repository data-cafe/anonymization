/*
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.config.v1;

import java.util.Arrays;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonValue;

public enum DBType {
    DB2("db2"),
    DB2_BLU("db2_blu"),
    EXCEL("excel"),
    FAKE("fake"),
    MYSQL("mysql"),
    ORACLE("oracle"),
    SQLITE("sqlite");

    @JsonValue
    private String type;

    DBType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    // Reverse Lookup ----------------------------------------------------------

    public static Optional<DBType> get(String type) {
        return Arrays.stream(DBType.values())
                .filter(env -> env.type.equals(type))
                .findFirst();
    }
}