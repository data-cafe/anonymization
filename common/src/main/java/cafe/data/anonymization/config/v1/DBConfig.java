/*
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.config.v1;

import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_EMPTY)
public class DBConfig {

    static private int DEFAULT_CHUNK_READ = 1000;
    static private int DEFAULT_CHUNK_WRITE = 10;

    // Type --------------------------------------------------------------------

    private DBType type;

    public DBType getType() {
        return type;
    }

    // Host --------------------------------------------------------------------

    private String host = null;

    public Optional<String> getHost() {
        return Optional.ofNullable(host);
    }

    // Port --------------------------------------------------------------------

    private Integer port = null;

    public Optional<Integer> getPort() {
        return Optional.ofNullable(port);
    }

    // Database ----------------------------------------------------------------

    private String database = null;

    public Optional<String> getDatabase() {
        return Optional.ofNullable(database);
    }

    // Username ----------------------------------------------------------------

    private String username = null;

    public Optional<String> getUsername() {
        return Optional.ofNullable(username);
    }

    // Password ----------------------------------------------------------------

    private String password = null;

    public Optional<String> getPassword() {
        return Optional.ofNullable(password);
    }

    // Schema ------------------------------------------------------------------

    private String schema = null;

    public Optional<String> getSchema() {
        return Optional.ofNullable(schema);
    }

    // File --------------------------------------------------------------------

    private String file = null;

    public Optional<String> getFile() {
        return Optional.ofNullable(file);
    }

    // Pagination: read & write ------------------------------------------------

    @JsonProperty("chunk_read")
    private Integer pageSizeRead = null;

    public int getPageSizeRead() {
        return Optional.ofNullable(pageSizeRead).orElse(DEFAULT_CHUNK_READ);
    }

    @JsonProperty("chunk_write")
    private Integer pageSizeWrite = null;

    public int getPageSizeWrite() {
        return Optional.ofNullable(pageSizeWrite).orElse(DEFAULT_CHUNK_WRITE);
    }

}