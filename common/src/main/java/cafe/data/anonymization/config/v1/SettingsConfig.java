/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.config.v1;

import java.io.IOException;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.apache.logging.log4j.Level;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_EMPTY)
public class SettingsConfig {

    // Log level ---------------------------------------------------------------

    @JsonProperty("log_level")
    @JsonSerialize(using = LevelSerializer.class)
    private Level logLevel;

    public Optional<Level> getLogLevel() {
        return Optional.ofNullable(logLevel);
    }

}

final class LevelSerializer extends JsonSerializer<Level> {

    @Override
    public void serialize(Level value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeString(value.name());
    }

}