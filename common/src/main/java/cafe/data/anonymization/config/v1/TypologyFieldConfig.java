/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.config.v1;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_EMPTY)
public class TypologyFieldConfig {

    // Field -------------------------------------------------------------------

    private String field;

    public String getField() {
        return field;
    }

    // Rule --------------------------------------------------------------------

    private RuleConfig rule;

    public RuleConfig getRule() {
        return rule;
    }

    // Select ------------------------------------------------------------------

    private String select;

    public String getSelect() {
        return select;
    }

    // Persist -----------------------------------------------------------------

    private boolean persist = false;

    public boolean isPersist() {
        return persist;
    }

}
