/*
 * Copyright(c)2022 Data Terrae
 */

package cafe.data.anonymization.config.v1;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_EMPTY)
public class Config {

    // Version -----------------------------------------------------------------

    private Integer version = null;

    public Integer getVersion() {
        return version;
    }

    // DB ----------------------------------------------------------------------

    private DBConfig db = new DBConfig();

    public DBConfig getDb() {
        return db;
    }

    // Dictionary ----------------------------------------------------------------

    private DictionaryConfig dictionary = new DictionaryConfig();

    public DictionaryConfig getDictionary() {
        return dictionary;
    }

    // Topologies ----------------------------------------------------------------

    private List<TopologyConfig> topologies = new ArrayList<TopologyConfig>();

    public List<TopologyConfig> getTopologies() {
        return topologies;
    }

    // Settings ----------------------------------------------------------------

    private SettingsConfig settings = new SettingsConfig();

    public SettingsConfig getSettings() {
        return settings;
    }

}
