/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.config.v1;

import java.beans.Transient;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_EMPTY)
public class DictionaryConfig {

    static private int DEFAULT_CHUNK_READ = 1000;
    static private int DEFAULT_CHUNK_WRITE = 10;

    static public String MEMORY = ":memory:";

    // Enable ------------------------------------------------------------------

    private Boolean enable = true;

    public boolean isEnable() {
        return enable == null ? false : enable;
    }

    // Path --------------------------------------------------------------------

    private String path = null;

    public Optional<String> getPath() {
        return Optional
                .ofNullable(path)
                .filter(p -> !MEMORY.equals(p));
    }

    @JsonGetter("path")
    private String getPathJson() {
        return path;
    }

    // Memory ------------------------------------------------------------------

    @Transient
    public boolean isInMemory() {
        return MEMORY.equals(path);
    }

    // Perpetual ---------------------------------------------------------------

    private boolean perpetual = false;

    public boolean isPerpetual() {
        return perpetual;
    }

    // Pagination: read & write ------------------------------------------------

    @JsonProperty("chunk_read")
    private Integer pageSizeRead = null;

    public int getPageSizeRead() {
        return Optional.ofNullable(pageSizeRead).orElse(DEFAULT_CHUNK_READ);
    }

    @JsonProperty("chunk_write")
    private Integer pageSizeWrite = null;

    public int getPageSizeWrite() {
        return Optional.ofNullable(pageSizeWrite).orElse(DEFAULT_CHUNK_WRITE);
    }
}
