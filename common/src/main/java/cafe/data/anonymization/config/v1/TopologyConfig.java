/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.config.v1;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_EMPTY)
public class TopologyConfig {

    // Table -------------------------------------------------------------------

    private String table;

    public String getTable() {
        return table;
    }

    // Identifier --------------------------------------------------------------

    private String identifier;

    public String getIdentifier() {
        return identifier;
    }

    // Fields ------------------------------------------------------------------

    private List<TypologyFieldConfig> fields = new ArrayList<TypologyFieldConfig>();

    public List<TypologyFieldConfig> getFields() {
        return fields;
    }

}
