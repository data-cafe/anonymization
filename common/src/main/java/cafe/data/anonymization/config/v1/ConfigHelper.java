/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.config.v1;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class ConfigHelper {
    static private final Logger LOGGER = LogManager.getLogger(ConfigHelper.class);

    /**
     * Load config from file
     * 
     * @param configPath relative path to config file
     * @return Deserialised config
     * 
     * @see IOException
     * @see StreamReadException
     * @see DatabindException
     */
    static public Config load(String configPath) throws Exception {
        // create object mapper instance
        ObjectMapper mapper = new ObjectMapper();

        // Locate config file
        File file = Paths.get(configPath).toFile();

        // convert JSON string to Config object
        return mapper.readValue(file, Config.class);
    }

    /**
     * Log config as DEBUG level (or as ERROR if serialisation fails)
     * 
     * @param config Config to log
     */
    static public void log(Config config) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new Jdk8Module());
            LOGGER.debug("Read config: {}", mapper.writeValueAsString(config));
        } catch (JsonProcessingException e) {
            LOGGER.error("Failed to log config");
            LOGGER.debug(e);
        }
    }

    private ConfigHelper() {
        throw new AssertionError("No instances");
    }

    static private List<DBType> needDBBasics = Arrays.asList(DBType.DB2, DBType.DB2_BLU, DBType.MYSQL, DBType.ORACLE);
    static private List<DBType> needDBSchema = Arrays.asList(DBType.DB2, DBType.DB2_BLU);
    static private List<DBType> needDBFile = Arrays.asList(DBType.SQLITE, DBType.EXCEL);

    /**
     * Check if the given config is well formed and valid, especially for DB config.
     * 
     * @param config The config to check
     * @throws RuntimeException If the config is not well formed or not valid
     */
    static public void check(Config config) throws RuntimeException {
        DBConfig db = config.getDb();

        // Check DB Host
        if (db.getHost().isEmpty() && needDBBasics.contains(db.getType())) {
            throw new RuntimeException("DB missing Host");
        }

        // Check DB Port
        if (db.getPort().isEmpty() && needDBBasics.contains(db.getType())) {
            throw new RuntimeException("DB missing Port");
        }

        // Check DB Username
        if (db.getUsername().isEmpty() && needDBBasics.contains(db.getType())) {
            throw new RuntimeException("DB missing Username");
        }

        // Check DB Password
        if (db.getPassword().isEmpty() && needDBBasics.contains(db.getType())) {
            throw new RuntimeException("DB missing Password");
        }

        // Check DB Database
        if (db.getDatabase().isEmpty() && needDBBasics.contains(db.getType())) {
            throw new RuntimeException("DB missing Database");
        }

        // Check DB Schema
        if (db.getSchema().isEmpty() && needDBSchema.contains(db.getType())) {
            throw new RuntimeException("DB missing Schema");
        }

        // Check DB File
        if (db.getFile().isEmpty() && needDBFile.contains(db.getType())) {
            throw new RuntimeException("DB missing File");
        }
    }
}
