/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.config.v1;

import java.util.Arrays;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonValue;

public enum RuleConfig {
    FIRST_NAME("first-name"),
    LAST_NAME("last-name"),
    EMAIL("email"),
    PHONE("phone");

    @JsonValue
    private String name;

    private RuleConfig(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    // Reverse Lookup ----------------------------------------------------------

    public static Optional<RuleConfig> get(String rule) {
        return Arrays.stream(RuleConfig.values())
                .filter(env -> env.name.equals(rule))
                .findFirst();
    }
}
