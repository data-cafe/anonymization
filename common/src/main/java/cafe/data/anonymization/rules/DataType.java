/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.rules;

public enum DataType {
    STRING,
    OBJECT;
}
