<!--
 Copyright (c) 2022 Data Terrae
-->

# Anonymization for CDC Habitat

## Prerequisites

### Needs to dev

- Java JDK 17+
- open your IDE in `/anonymization` folder (not in root of the project)
- for VSCode: install recommanded extensions

### Needs to devops

- Gradle (but overrided by `Gradle Wrapper`)

## How to code?

### Where to code?

- For **common** code (shared):
  - code is in [common/src/main/java/](common/src/main/java/) directory
  - tests are in [common/src/test/java/](common/src/test/java/) directory
- For the **runner**:
  - code is in [runner/src/main/java/](runner/src/main/java/) directory
  - tests are in [runner/src/test/java/](runner/src/test/java/) directory
- For the **dictionary**:
  - code is in [dictionary/src/main/java/](dictionary/src/main/java/) directory
  - tests are in [dictionary/src/test/java/](dictionary/src/test/java/) directory

Note: the **dictionary** is a lib dependency of the **runner**.

### Where to add dependencies?

- For **common** (but not shared), in [common/build.gradle](common/build.gradle) file
- For the **runner**, in [runner/build.gradle](runner/build.gradle) file
- For the **dictionary**, in [dictionary/build.gradle](dictionary/build.gradle) file

### How to get config?

TBD

### How to use logger?

You need to put one logger for each file (class).

```java
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyClass {
    private static final Logger LOGGER = LoggerFactory.getLogger(MyClass.class); // always FIRST

    public void doSomething() {
        LOGGER.info("Hello World!");
    }
}
```

## Useful commands

### Run the app

:warning: **DO NO FORGET** the [configuration](#configuration) file.

```sh
anonymization$ ./gradlew run --args="--config ../config.sample.json"
```

### Release

```sh
anonymization$ ./gradlew build
```

### Clean the project

```sh
anonymization$ ./gradlew clean
```

## Configuration

### Run with config

You have two possibilities to run the app with a custom configuration:

- a `config.json` file is just aside the app
- `--config path/to/config.json` arg is given to the app

### Config specs

Take a look in the [Wiki](https://gitlab.com/data-terrae/projects/cdc-habitat/-/wikis/Anonymization/Config).
