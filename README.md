<!--
 Copyright (c) 2022 Data Terrae
-->

# Anonymization brick for data·café

This a single app that anonymizes data from the [CDC Habitat](https://www.cdc-habitat.fr/) dataset.

## For Data Terrae' developers

Take a look at [contributor](CONTRIBUTION.md) page.

Technical requirements:

- JVM version: **17+**
- CPU: **TBD**
- RAM: **TBD**

Here the simple steps to use the app:

1. get the last version of the app
2. create a `config.json` file aside the app
3. simply run the app
