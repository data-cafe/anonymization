/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.runner;

import java.io.FileNotFoundException;
import java.nio.file.Paths;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

import cafe.data.anonymization.config.v1.Config;
import cafe.data.anonymization.config.v1.ConfigHelper;
import cafe.data.anonymization.orchestrator.Orchestrator;

public class Runner {
    private static final Logger LOGGER = LogManager.getLogger(Runner.class);

    // ARG: Config -------------------------------------------------------------
    static String ARG_CONFIG_SHORT = "c";
    static String ARG_CONFIG_LONG = "config";
    static String ARG_CONFIG_DEFAULT = "./config.json";

    // MAIN --------------------------------------------------------------------

    public static void main(String[] args) {
        // Get config path
        String configPath;
        try {
            CommandLine cmd = argsParser(args);
            configPath = cmd.getOptionValue(ARG_CONFIG_LONG, ARG_CONFIG_DEFAULT);
        } catch (ParseException e) {
            LOGGER.info("Failed to parse command line arguments. Don't worry, we will use default values");
            LOGGER.debug(e);
            configPath = ARG_CONFIG_DEFAULT;
        }
        LOGGER.debug("Config path: {}", configPath);

        // Load config
        Config config;
        try {
            config = ConfigHelper.load(configPath);
        } catch (Exception e) {
            config = null;
            if (e instanceof FileNotFoundException) {
                LOGGER.error("Failed to load config from {}", Paths.get(configPath).toAbsolutePath().toString());
            }
            LOGGER.error("Failed to load config file. Exiting...");
            LOGGER.debug(e);
            System.exit(1);
        }

        // Log read config (if debug activated)
        if (LOGGER.getLevel().isLessSpecificThan(Level.DEBUG)) {
            ConfigHelper.log(config);
        }

        // Check config
        try {
            ConfigHelper.check(config);
        } catch (RuntimeException e) {
            LOGGER.error("Config file is not valid. Exiting...");
            LOGGER.error(e.getMessage());
            System.exit(1);
        }

        // Set log level
        config.getSettings().getLogLevel().ifPresentOrElse(level -> {
            Configurator.setRootLevel(level);
            LOGGER.fatal("Log level set to {}", level);
        }, () -> {
            LOGGER.fatal("Log level is actually set to {} (not superseeded by config)", LOGGER.getLevel());
        });

        // Create instances
        Orchestrator orchestrator = new Orchestrator(config);

        // Initialize
        orchestrator.init();

        // Launch process
        try {
            orchestrator.startAnonymization();
        } catch (Exception e) {
            LOGGER.error("An error occurred during anonymization process. Exiting...", e);
        }

        // Clean all
        LOGGER.debug("Cleaning all...");
        orchestrator.destroy();

        // Finish program
        System.exit(0);
    }

    // Helpers -----------------------------------------------------------------

    /**
     * Parses the command line arguments.
     * 
     * @param args the command line arguments given to the main app
     * @return A build command line object
     * @throws Exception
     */
    public static CommandLine argsParser(String[] args) throws ParseException {

        Options options = new Options();

        Option input = new Option(ARG_CONFIG_SHORT, ARG_CONFIG_LONG, true, "config file path");
        input.setRequired(false);
        options.addOption(input);

        CommandLineParser parser = new DefaultParser();

        return parser.parse(options, args);
    }
}
