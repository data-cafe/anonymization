/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.orchestrator;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cafe.data.anonymization.config.v1.Config;
import cafe.data.anonymization.dictionary.Dictionary;
import cafe.data.anonymization.flow.Item;
import cafe.data.anonymization.flow.Page;
import cafe.data.anonymization.flow.Topology;
import cafe.data.anonymization.origin.Origin;

public class Orchestrator {
    private static final Logger LOGGER = LogManager.getLogger(Orchestrator.class);

    final private Config config;
    final private Origin origin;
    final private Dictionary dictionary;
    final List<Topology> topologies;

    // Lifecycle ---------------------------------------------------------------

    public Orchestrator(Config config) {
        LOGGER.debug("Creating...");

        // Parameters
        this.config = config;

        // Layers
        this.origin = new Origin();
        this.dictionary = new Dictionary();

        // Topologies
        LOGGER.debug("Creating topologies...");
        topologies = config.getTopologies()
                .parallelStream()
                .map(Topology::fromConfig)
                .collect(Collectors.toList());

        LOGGER.debug("Creating... DONE");
    }

    public void init() {
        LOGGER.debug("Initializing...");

        origin.init(config);
        dictionary.init(config, topologies);

        LOGGER.debug("Initializing... DONE");
    }

    public void destroy() {
        LOGGER.debug("Destroying...");

        // Unlik data
        topologies.clear();

        // Detroy layers
        dictionary.destroy();
        origin.destroy();

        LOGGER.debug("Destroying... DONE");
    }

    // Public ------------------------------------------------------------------

    public void startAnonymization() {
        LOGGER.info("🏃 Anonymization starting...");
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        // TODO: parappelize this
        // Consequence: the data layer has to be able to read and write in parallel
        // Consequence: the dictionary has to be able to read and write in parallel

        topologies.forEach(topology -> {
            // First pagination
            Page page = new Page(0, config.getDb().getPageSizeRead());

            // Launch process
            LOGGER.info("┬ Start with topology {} ({}) with step of {} items",
                    topology.getDbTable(), topology.getDictTable(), config.getDb().getPageSizeRead());
            runChunk(topology, page);

            // Waiting all actions finished
            while (origin.getQueuedActions() > 0) {
                try {
                    LOGGER.info("│ ⌛ Waiting {} DB updates", origin.getQueuedActions());
                    LOGGER.debug("│ ⌛ Waiting {} dictionary persistencies", dictionary.getQueuedActions());
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    LOGGER.error("└ Error while waiting for DB updates to finish", e);
                }
            }

            LOGGER.info("└ Done with topology {} ({})", topology.getDbTable(), topology.getDictTable());
        });

        // Display timer
        stopWatch.stop();
        LOGGER.info("🏁 Anonymization is FINISHED in {} days, {} hours, {} minutes, {} seconds",
                stopWatch.getTime(TimeUnit.DAYS),
                stopWatch.getTime(TimeUnit.HOURS) % 24,
                stopWatch.getTime(TimeUnit.MINUTES) % 60,
                stopWatch.getTime(TimeUnit.SECONDS) % 60);

        if (dictionary.getQueuedActions() > 0) {
            LOGGER.info("🏃 Finishing the dictionary persistency...");

            // Waiting dictionary finished
            while (dictionary.getQueuedActions() > 0 || origin.getQueuedActions() > 0) {
                try {
                    LOGGER.info("• ⌛ Waiting {} dictionary persistencies", dictionary.getQueuedActions());
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    LOGGER.error("• Error while waiting dictionary persistency to finish", e);
                }
            }
            LOGGER.info("🏁 Whole process (anonymization and percistancy) is finished!");
        }
    }

    // Private -----------------------------------------------------------------

    private void runChunk(Topology topology, Page page) {

        // Read items
        // Chunk: use the config->db->chunkSizeRead
        List<Item> items = origin.read(topology, page);

        // Generate identifiers
        items.forEach(Item::computeIdentifier);

        // Anonymize
        // Chunk: use the config->dictionary->chunkSizeRead
        // Chunk: use the config->dictionary->chunkSizeWrite
        dictionary.anonymize(items);

        // Update items
        origin.update(items);

        LOGGER.info("├ {}: {} items treated (page {} to {})",
                topology.getDbTable(), items.size(), page.getStart(), page.getEnd());

        // Loop: process next page
        if (items.size() >= config.getDb().getPageSizeRead()) {
            runChunk(topology, page.nextPage());
        }

        // Help the garbage collector
        // Item's content is not deleted because we do not know, yet, if the threads
        // (persist::save and connector::update) are finished.
        page = null;
    }

}
