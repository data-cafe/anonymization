/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.origin;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import com.google.common.collect.Lists;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cafe.data.anonymization.config.v1.Config;
import cafe.data.anonymization.config.v1.DBConfig;
import cafe.data.anonymization.flow.Item;
import cafe.data.anonymization.flow.Page;
import cafe.data.anonymization.flow.Topology;

public abstract class Connector {

    // Properties --------------------------------------------------------------
    protected final Logger LOGGER;

    protected Config globalConfig;
    protected DBConfig config;

    /**
     * Thread execurtor to update items
     */
    private final ThreadPoolExecutor executor;

    // Lifecycle ---------------------------------------------------------------

    protected Connector(Class<? extends Connector> clazz, Config config, int updateThreads) {
        // Specific logger
        this.LOGGER = LogManager.getLogger(clazz);
        LOGGER.debug("Creating...");

        // Link config
        this.globalConfig = config;
        this.config = config.getDb();

        // Executor
        this.executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(updateThreads);

    }

    public void destroy() {
        LOGGER.debug("Destroying...");

        // Shutdown executor
        executor.shutdown();

        // Unlink config
        this.globalConfig = null;
        this.config = null;
    }

    // Public API --------------------------------------------------------------

    public abstract List<Map<String, Object>> read(Topology topology, Page page);

    public abstract Collection<Runnable> createUpdateTask(Collection<Item> items);

    public void update(List<Item> items) {
        // Add items to update
        Lists.partition(items, config.getPageSizeWrite())
                .parallelStream()
                .map(this::createUpdateTask)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .forEach(executor::execute);
    }

    public int getQueuedActions() {
        return executor.getQueue().size();
    }
}
