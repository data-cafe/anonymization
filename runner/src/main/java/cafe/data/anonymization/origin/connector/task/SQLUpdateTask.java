/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.origin.connector.task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cafe.data.anonymization.flow.Item;
import cafe.data.anonymization.flow.Topology;

public class SQLUpdateTask implements Runnable {
    private final Logger LOGGER = LogManager.getLogger(SQLUpdateTask.class);

    /**
     * The simple SQL query to execute.
     */
    private static final String QUERY = "UPDATE %s SET %s WHERE %s;";

    /**
     * Connection used to update items.
     * Note: the connection is supposed to be reused and writeable.
     */
    private final Connection connection;

    /**
     * Limit the query request time.
     * 
     * The timeout is in seconds.
     */
    private final int queryTimeout;

    private final String queryFieldEncapsulator;

    /**
     * Target items
     */
    private final Collection<Item> items;

    public SQLUpdateTask(Connection connection, int queryTimeout, String queryFieldEncapsulator,
            Collection<Item> items) {
        this.connection = connection;
        this.queryTimeout = queryTimeout;
        this.queryFieldEncapsulator = queryFieldEncapsulator;
        this.items = items;
    }

    @Override
    public void run() {
        Pair<String, Map<Integer, Function<Item, String>>> templates = templateQuery(
                items.iterator().next().getTopology());
        String templateQuery = templates.getLeft();
        Map<Integer, Function<Item, String>> templateFunctions = templates.getRight();

        try {
            PreparedStatement statement = this.connection.prepareStatement(templateQuery);
            statement.setQueryTimeout(queryTimeout);

            // Fill items
            for (Item item : items) {
                // Fill all values
                for (Map.Entry<Integer, Function<Item, String>> entry : templateFunctions.entrySet()) {
                    statement.setString(
                            /* counter */ entry.getKey().intValue(),
                            /* value */ entry.getValue().apply(item));
                }

                // Validate batch
                statement.addBatch();
            }

            // Execute all updates
            statement.executeBatch();

            statement.close();
        } catch (SQLException e) {
            LOGGER.error("Error querying database", e);
        }
    }

    // Helper ------------------------------------------------------------------

    /**
     * It should return two elements:
     * 1. the template query
     * 2. the associate list of functions to call for filling the template query
     * 
     * The template query should look like:
     * UPDATE table SET field1 = ?, field2 = ? WHERE field3 = ? AND field4 = ?;
     * 
     * The map should look like:
     * 1 ==> item -> item.getAnonymizedValue(field)
     * 2 ==> item -> item.getAnonymizedValue(field)
     * 3 ==> item -> item.getOriginalValue(field)
     * 4 ==> item -> item.getOriginalValue(field)
     * 
     * @param topology The topology to generate the template query.
     * @return A pair containing the template query and a map of the template
     *         parameter functions.
     */
    private Pair<String, Map<Integer, Function<Item, String>>> templateQuery(Topology topology) {
        Map<Integer, Function<Item, String>> fieldToQuery = new HashMap<>();
        AtomicInteger counter = new AtomicInteger(0);

        // Set net anonymized values
        // i.e. SET `field` = ? (<-- value position)
        List<String> assignations = new LinkedList<>();
        topology.getAnonFields().forEach(field -> {
            assignations.add(queryFieldEncapsulator.formatted(field.getDbField()) + "= ?");
            fieldToQuery.put(
                    /* index of ? position */counter.incrementAndGet(),
                    /* lambda: item to value */ item -> item.getAnonymizedValue(field).toString());
        });

        // Identify the item in DB
        // i.e. WHERE `id` = ? (<-- value position)
        List<String> identifications = new LinkedList<>();
        topology.getIdFields().forEach(field -> {
            identifications.add(queryFieldEncapsulator.formatted(field.getDbField()) + "= ?");
            fieldToQuery.put(
                    /* index of ? position */counter.incrementAndGet(),
                    /* lambda: item to value */ item -> item.getOriginalValue(field).toString());
        });

        return new ImmutablePair<>(
                QUERY.formatted(
                        /* 1: table */ topology.getDbTable(),
                        /* 2: assignations */ String.join(", ", assignations),
                        /* 3: identifications */ String.join(" AND ", identifications)),
                fieldToQuery);
    }

}
