/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.origin.connector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import cafe.data.anonymization.config.v1.Config;
import cafe.data.anonymization.flow.Field;
import cafe.data.anonymization.flow.Item;
import cafe.data.anonymization.flow.Page;
import cafe.data.anonymization.flow.Topology;
import cafe.data.anonymization.origin.Connector;
import cafe.data.anonymization.origin.connector.helper.SQLHelper;
import cafe.data.anonymization.origin.connector.task.SQLUpdateTask;

public class SQLiteConnector extends Connector {

    /**
     * Time limit for query for each item.
     * So, you are supposed to multiply this value with the number of items.
     * 
     * The timeout is in seconds.
     */
    private static final int UNITARY_QUERY_TIMEOUT = 1;

    /**
     * Reusable connection to SQLite for whole treatment.
     */
    private Connection connection = null;

    // Lifecycle ---------------------------------------------------------------

    public SQLiteConnector(Config config) {
        super(SQLiteConnector.class, config, 1);

        String path = config.getDb().getFile()
                .orElseThrow(() -> new IllegalStateException("No file specified"));

        try {
            // create a database connection
            connection = DriverManager.getConnection("jdbc:sqlite:" + path);
        } catch (SQLException e) {
            // if the error message is "out of memory",
            // it probably means no database file is found
            LOGGER.error("Error connecting to database", e);
        }
    }

    @Override
    public void destroy() {
        // Close DB connection
        if (this.connection != null) {
            try {
                this.connection.close();
            } catch (SQLException e) {
                LOGGER.error("Error closing DB connection", e);
            }
        }

        // Unload driver
        this.connection = null;

        // Super
        super.destroy();
    }

    // Public API --------------------------------------------------------------

    @Override
    public List<Map<String, Object>> read(Topology topology, Page page) {

        // Exhaustive list of all needed fields:
        // - fields used for primary key
        // - fields to anonymize
        List<Field> neededFields = topology.getFields(true, false, true);

        try {
            // Build query
            Statement statement = this.connection.createStatement();
            statement.setQueryTimeout(UNITARY_QUERY_TIMEOUT * page.getSize());

            String selectPart = neededFields.parallelStream()
                    .map(Field::getDbField)
                    .distinct()
                    .map(SQLHelper.BACKQUOTE_ENCAPSULATOR::formatted)
                    .collect(Collectors.joining(", "));

            String query = "SELECT " + selectPart + " " +
                    "FROM " + topology.getDbTable() + " " +
                    "LIMIT " + page.getStart() + ", " + page.getSize() + ";";
            LOGGER.debug("Load: " + query);

            ResultSet rs = statement.executeQuery(query);

            List<Map<String, Object>> result = new ArrayList<>();
            while (rs.next()) {
                Map<String, Object> raw = neededFields.parallelStream().collect(Collectors.toMap(
                        Field::getDbField,
                        field -> {
                            try {
                                switch (field.getType().getDataType()) {
                                    case STRING:
                                        return rs.getString(field.getDbField());
                                    case OBJECT:
                                    default:
                                        return rs.getObject(field.getDbField());
                                }
                            } catch (SQLException e1) {
                                return null;
                            }
                        }));
                result.add(raw);
            }

            statement.close();

            LOGGER.debug(result);

            return result;

        } catch (SQLException e) {
            LOGGER.error("Error querying database", e);
            return Collections.emptyList();
        }

    }

    @Override
    public Collection<Runnable> createUpdateTask(Collection<Item> items) {
        return List.of(new SQLUpdateTask(
                connection,
                UNITARY_QUERY_TIMEOUT * items.size(),
                SQLHelper.BACKQUOTE_ENCAPSULATOR,
                items));
    }

}
