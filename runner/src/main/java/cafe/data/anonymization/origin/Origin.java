/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.origin;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cafe.data.anonymization.config.v1.Config;
import cafe.data.anonymization.flow.Item;
import cafe.data.anonymization.flow.Page;
import cafe.data.anonymization.flow.Topology;
import cafe.data.anonymization.origin.connector.DB2Connector;
import cafe.data.anonymization.origin.connector.FakeConnector;
import cafe.data.anonymization.origin.connector.SQLiteConnector;

public class Origin {
    private static final Logger LOGGER = LogManager.getLogger(Origin.class);

    // Internal properties -----------------------------------------------------

    private Connector connector;

    // Lifecycle ---------------------------------------------------------------

    public Origin() {
        LOGGER.debug("Creating...");
    }

    public void init(Config config) {
        LOGGER.debug("Initializing...");

        // Load connector
        switch (config.getDb().getType()) {
            case DB2:
            case DB2_BLU:
                this.connector = new DB2Connector(config);
                break;
            case EXCEL:
                throw new RuntimeException("EXCEL not supported yet");
            case FAKE:
                this.connector = new FakeConnector(config);
                break;
            case MYSQL:
                throw new RuntimeException("MYSQL not supported yet");
            case ORACLE:
                throw new RuntimeException("ORACLE not supported yet");
            case SQLITE:
                this.connector = new SQLiteConnector(config);
                break;
            default:
                throw new RuntimeException("Unknown database type: " + config.getDb().getType());
        }
    }

    public void destroy() {
        LOGGER.debug("Destroying...");

        // Unlink connector
        this.connector.destroy();
        this.connector = null;
    }

    // Public methods ----------------------------------------------------------

    public List<Item> read(Topology topology, Page page) {
        // Request items from DB for pagination
        List<Map<String, Object>> items = this.connector.read(topology, page);

        // Encapsulate read items into our items
        List<Item> flowItems = items
                .parallelStream()
                .map(item -> new Item(topology, item))
                .collect(Collectors.toList());

        return flowItems;
    }

    public void update(List<Item> items) {
        this.connector.update(items);
    }

    public int getQueuedActions() {
        return this.connector.getQueuedActions();
    }
}
