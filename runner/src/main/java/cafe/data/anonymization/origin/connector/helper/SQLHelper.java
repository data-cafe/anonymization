/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.origin.connector.helper;

public final class SQLHelper {

    /**
     * Do not encapsulate the value.
     */
    static public final String BLANK_ENCAPSULATOR = "%s";

    /**
     * Encapsulate a value with back-quotes.
     */
    static public final String BACKQUOTE_ENCAPSULATOR = "`%s`";

    /**
     * Encapsulate a value with single-quotes.
     */
    static public final String SINGLEQUOTE_ENCAPSULATOR = "'%s'";

    /**
     * Encapsulate a value with double-quotes.
     */
    static public final String DOUBLEQUOTE_ENCAPSULATOR = "\"%s\"";

    // Singleton ---------------------------------------------------------------

    private SQLHelper() {
        throw new IllegalStateException("Utility class");
    }
}
