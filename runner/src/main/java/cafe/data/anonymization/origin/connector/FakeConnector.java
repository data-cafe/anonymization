/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.origin.connector;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import cafe.data.anonymization.config.v1.Config;
import cafe.data.anonymization.flow.Field;
import cafe.data.anonymization.flow.Item;
import cafe.data.anonymization.flow.Page;
import cafe.data.anonymization.flow.Topology;
import cafe.data.anonymization.origin.Connector;

public class FakeConnector extends Connector {

    // Lifecycle ---------------------------------------------------------------

    public FakeConnector(Config config) {
        super(FakeConnector.class, config, 1);

        // Do nothing, that's fake data
    }

    @Override
    public void destroy() {
        // Do nothing, that's fake data

        // Super
        super.destroy();
    }

    // Public API --------------------------------------------------------------

    @Override
    public List<Map<String, Object>> read(Topology topology, Page page) {
        // Exhaustive list of all needed fields:
        // - fields used for primary key
        // - fields to anonymize
        List<Field> neededFields = topology.getFields(true, true, false);

        return Stream
                .of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                .map(index -> fake(index, neededFields))
                .collect(Collectors.toList());
    }

    @Override
    public Collection<Runnable> createUpdateTask(Collection<Item> items) {
        // Do nothing, that's fake data
        return null;
    }

    // Helpers -----------------------------------------------------------------

    private Map<String, Object> fake(int index, List<Field> neededFields) {
        return neededFields.stream().collect(Collectors.toMap(
                field -> field.getDbField(),
                field -> {
                    switch (field.getType()) {
                        case IDENTIFY:
                            return String.valueOf(index);
                        default:
                            return field.getDbField() + "-" + index;
                    }
                }));
    }

}
