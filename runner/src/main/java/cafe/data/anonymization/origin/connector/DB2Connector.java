/**
 * Copyright (c) 2022 Data Terrae
 */

package cafe.data.anonymization.origin.connector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import cafe.data.anonymization.config.v1.Config;
import cafe.data.anonymization.flow.Field;
import cafe.data.anonymization.flow.Item;
import cafe.data.anonymization.flow.Page;
import cafe.data.anonymization.flow.Topology;
import cafe.data.anonymization.origin.Connector;
import cafe.data.anonymization.origin.connector.helper.SQLHelper;
import cafe.data.anonymization.origin.connector.task.SQLUpdateTask;

public class DB2Connector extends Connector {
    private static final String CONNECTION_URL = "jdbc:db2://%s:%d/%s:currentSchema=%s;";
    private static final String READ_QUERY = "SELECT %s FROM %s LIMIT %d, %d;";

    /**
     * Time limit for query for each item.
     * So, you are supposed to multiply this value with the number of items.
     * 
     * The timeout is in seconds.
     */
    private static final int UNITARY_QUERY_TIMEOUT = 1;

    private Connection connection = null;

    // Lifecycle ---------------------------------------------------------------

    public DB2Connector(Config config) {
        super(DB2Connector.class, config, 1);

        // Get all config from config file
        // Note: requirements are checked by ConfigHelper.check() in Runner phase
        String host = this.config.getHost().get();
        int port = this.config.getPort().get();
        String database = this.config.getDatabase().get();
        String schema = this.config.getSchema().get();
        String username = this.config.getUsername().get();
        String password = this.config.getPassword().get();

        try {
            // Load the DB2Driver class
            Class.forName("com.ibm.db2.jcc.DB2Driver");

            // Connection to DB2 database using connector
            String url = CONNECTION_URL.formatted(host, port, database, schema);
            connection = DriverManager.getConnection(url, username, password);

        } catch (SQLException e) {
            LOGGER.error("Error connecting to the database", e);

        } catch (ClassNotFoundException e) {
            LOGGER.error("Error loading the DB2Driver connector", e);
        }
    }

    @Override
    public void destroy() {
        // Close DB connection
        try {
            this.connection.close();
        } catch (Exception e) {
            LOGGER.debug("Error closing DB connection", e);
        }

        // Unlink DB connection
        this.connection = null;

        // Super
        super.destroy();
    }

    // Public API --------------------------------------------------------------

    @Override
    public List<Map<String, Object>> read(Topology topology, Page page) {

        // Get table's entrypoint to request
        String table = topology.getDbTable();

        // Exhaustive list of all needed fields:
        // - fields used for primary key
        // - fields to anonymize
        List<Field> neededFields = topology.getFields(true, false, true);

        try {
            // Build query
            Statement statement = this.connection.createStatement();
            statement.setQueryTimeout(UNITARY_QUERY_TIMEOUT * page.getSize());

            String selectPart = neededFields.parallelStream()
                    .map(Field::getDbField)
                    .collect(Collectors.joining(", "));

            String query = READ_QUERY.formatted(selectPart, table, page.getStart(), page.getSize());
            LOGGER.debug(String.format("Load: %s", query));

            ResultSet rs = statement.executeQuery(query);

            List<Map<String, Object>> result = new ArrayList<>();
            while (rs.next()) {
                Map<String, Object> raw = neededFields.parallelStream().collect(Collectors.toMap(
                        Field::getDbField,
                        field -> {
                            try {
                                switch (field.getType().getDataType()) {
                                    case STRING:
                                        return rs.getString(field.getDbField());
                                    case OBJECT:
                                    default:
                                        return rs.getObject(field.getDbField());
                                }
                            } catch (SQLException e1) {
                                return null;
                            }
                        }));
                result.add(raw);
            }
            statement.close();

            LOGGER.debug(result);

            return result;

        } catch (SQLException e) {
            LOGGER.error("Error querying database", e);
            return Collections.emptyList();

        }
    }

    @Override
    public Collection<Runnable> createUpdateTask(Collection<Item> items) {
        return List.of(new SQLUpdateTask(
                connection,
                UNITARY_QUERY_TIMEOUT * items.size(),
                SQLHelper.BLANK_ENCAPSULATOR,
                items));
    }

}
